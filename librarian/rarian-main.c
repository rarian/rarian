/*
 * rarian-main.c
 * This file is part of Rarian
 *
 * Copyright (C) 2006 - Don Scorgie
 *
 * Rarian is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rarian is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <ctype.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "config.h"
#include "rarian-ht-utils.h"
#include "rarian-language.h"
#include "rarian-list-utils.h"
#include "rarian-reg-utils.h"
#include "rarian-utils.h"
#include "rarian.h"
#if ENABLE_OMF_READ
#include "rarian-omf.h"
#endif

#ifndef FALSE
#define FALSE 0
#define TRUE !FALSE
#endif

/* Internal structures and lists */

typedef struct _Link Link;

struct _Link {
    union {
        RrnReg *reg;
        RrnSect *sect;
    } reg;
    Link *next;
    Link *prev;
};

static Link *head = NULL;
static Link *tail = NULL;

static Link *orphans_head = NULL;
static Link *orphans_tail = NULL;

/* Function Prototypes */

static void rrn_init(void);
static void scan_directories(void);
static void scan_directory(const char *dir);
static void process_file(const char *filename);
static void process_section(const char *filename);
static void insert_orphans(void);
static void reverse_children(void);
static void process_locale_dirs(const char *dir);
#if ENABLE_OMF_READ
static void process_omf_dir(char *dir);
#endif

static void reg_link_free(Link *link) {
    rrn_reg_free(link->reg.reg);
    free(link);
}

static void sect_link_free(Link *link) {
    rrn_sect_free(link->reg.sect);
    free(link);
}

void rrn_set_language(char *lang_code) {
    rrn_shutdown();
    rrn_language_init(lang_code);
    rrn_init();
}

static void rrn_init(void) {
    if (head)
        return;

    scan_directories();

    return;
}

void rrn_for_each(RrnForeachFunc funct, void *user_data) {
    Link *iter;

    rrn_init();

    iter = head;

    while (iter) {
        int res;
        res = funct(iter->reg.reg, user_data);
        if (res == FALSE)
            break;
        iter = iter->next;
    }

    return;
}

void rrn_for_each_in_category(RrnForeachFunc funct, char *category,
                              void *user_data) {
    Link *iter;

    rrn_init();
    iter = head;

    while (iter) {
        int res;
        char **cats;

        cats = iter->reg.reg->categories;
        while (cats && *cats) {
            if (!strcmp(*cats, category)) {
                res = funct(iter->reg.reg, user_data);
                if (res == FALSE)
                    break;
            }
            cats++;
        }
        iter = iter->next;
    }

    return;
}

RrnReg *rrn_find_entry_from_uri(char *uri) {
    Link *iter;

    rrn_init();
    iter = head;

    while (iter) {
        if (!strcmp(iter->reg.reg->uri, uri))
            return iter->reg.reg;
        iter = iter->next;
    }

    return NULL;
}

static void scan_directories(void) {

#if ENABLE_INSTALL
    char *path = NULL;
    char **scan_dirs = NULL;
    char *home_dir = NULL;
    char *home_data_dir = NULL;
    char *home_env = NULL;

    home_env = getenv("XDG_DATA_HOME");
    if (home_env)
        home_data_dir = strdup(home_env);

    if (!home_data_dir || !strcmp(home_data_dir, "")) {
        free(home_data_dir);
        home_data_dir = NULL;
        home_env = getenv("HOME");
        if (!home_env || !strcmp(home_env, "")) {
            fprintf(stderr, "Warning: HOME dir is not defined."
                            "  Skipping check of XDG_DATA_HOME");
            goto past;
        }
        home_data_dir = malloc(sizeof(char) * (strlen(home_env) + 14));
        sprintf(home_data_dir, "%s/.local/share", home_env);
    }

    home_dir = malloc(sizeof(char) * (strlen(home_data_dir) + 6));

    sprintf(home_dir, "%s/help", home_data_dir);

#if ENABLE_OMF_READ
    process_omf_dir(home_data_dir);
#endif

    free(home_data_dir);
    home_data_dir = NULL;

    process_locale_dirs(home_dir);
    scan_directory(home_dir);

    free(home_dir);
    home_dir = NULL;

past:
    path = getenv("XDG_DATA_DIRS");

    if (!path || !strcmp(path, "")) {
        path = "/usr/local/share/:/usr/share/";
    }
    scan_dirs = rrn_str_split(path, ':');

    for (char **cur_path = scan_dirs; cur_path && *cur_path; cur_path++) {
        char *check_path = rrn_strconcat(*cur_path, "/help", NULL);
#if ENABLE_OMF_READ
        process_omf_dir(*cur_path);
#endif
        process_locale_dirs(check_path);

        scan_directory(check_path);
        free(check_path);
    }
#else
    const char *cur_path = "data/sk-import";
    process_locale_dirs(cur_path);
    scan_directory(cur_path);
#endif
    reverse_children();
}

static void process_locale_dirs(const char *dir) {
    char **paths_to_check = NULL;
    char **iter = NULL;

    /* rrn_language_get_dirs() doesn't modify the param. */
    paths_to_check = rrn_language_get_dirs((char *)dir);
    iter = paths_to_check;

    while (*iter) {
        scan_directory(*iter);
        free(*iter);
        iter++;
    }
    free(paths_to_check);
}

static void scan_directory(const char *dir) {
    struct dirent *dp = NULL;
    struct stat buf;

    if (access(dir, R_OK)) {
        return;
    }

    DIR *dirp = opendir(dir);
    if (!dirp)
        return;

    while ((dp = readdir(dirp)) != NULL) {
        char *full_name = rrn_strconcat(dir, "/", dp->d_name, NULL);

        stat(full_name, &buf);

        if (S_ISREG(buf.st_mode)) {
            char *suffix = NULL;

            suffix = strrchr(full_name, '.');
            if (suffix) {
                if (!strcmp(suffix, ".document")) {
                    process_file(full_name);
                } else if (!strcmp(suffix, ".section")) {
                    process_section(full_name);
                }
            }
        } else if (S_ISDIR(buf.st_mode) && strcmp(dp->d_name, ".") &&
                   strcmp(dp->d_name, "..") && strcmp(dp->d_name, "LOCALE")) {
            scan_directory(full_name);
        }
        free(full_name);
    }

    insert_orphans();
    closedir(dirp);
}

static int handle_duplicate(RrnReg *reg) {
    Link *iter;

    iter = head;

    while (iter) {
        if ((iter->reg.reg->heritage && reg->heritage &&
             !strcmp(iter->reg.reg->heritage, reg->heritage)) ||
            !strcmp(iter->reg.reg->identifier, reg->identifier)) {
            if (iter->reg.reg->lang && reg->lang &&
                rrn_language_use(iter->reg.reg->lang, reg->lang)) {
                rrn_reg_free(iter->reg.reg);
                iter->reg.reg = reg;
            }
            return TRUE;
        }
        iter = iter->next;
    }

    return FALSE;
}

#if ENABLE_OMF_READ
static void process_omf_dir(char *dir) {
    char *path;
    DIR *dirp = NULL;
    char **langs = NULL;
    int lang_found = FALSE;
    int lang_count = 0;
    char *tmp = NULL;

    struct dirent *dp = NULL;
    struct stat buf;

    langs = rrn_language_get_langs();
    path = rrn_strconcat(dir, "/omf", NULL);

    if (access(path, R_OK)) {
        free(path);
        return;
    }
    dirp = opendir(path);
    if (!dirp) {
        free(path);
        return;
    }

    for (char **langs_iter = langs; *langs_iter; langs_iter++) {
        lang_count++;
        if (!strcmp(*langs_iter, "C")) {
            lang_found = TRUE;
        }
    }

    if (!lang_found) {
        langs = realloc(langs, sizeof(char *) * (lang_count + 2));
        langs[lang_count] = strdup("C");
        langs[lang_count + 1] = NULL;
    }

    while ((dp = readdir(dirp)) != NULL) {

        char *full_name = rrn_strconcat(path, "/", dp->d_name, NULL);
        stat(full_name, &buf);
        free(full_name);
        if (S_ISDIR(buf.st_mode) && strcmp(dp->d_name, ".") &&
            strcmp(dp->d_name, "..")) {
            for (char **langs_iter = langs; *langs_iter; langs_iter++) {
                char *lang = (*langs_iter);
                char *omf_file =
                    rrn_strconcat(path, "/", dp->d_name, "/", dp->d_name, "-",
                                  lang, ".omf", NULL);

                if (!access(omf_file, R_OK)) {
                    RrnReg *reg = rrn_omf_parse_file(omf_file);
                    if (reg) {
                        reg->omf_location = strdup(omf_file);
                        reg->ghelp_name = strdup(dp->d_name);
                        if (!handle_duplicate(reg)) {
                            Link *link;

                            link = malloc(sizeof(Link));
                            link->reg.reg = reg;
                            link->next = NULL;

                            RRN_HT_APPEND(head, tail, link);
                        }
                    }
                }
                free(omf_file);
            }
        }
    }

    free(langs);
    free(path);
    insert_orphans();
    closedir(dirp);
}
#endif

static void process_section(const char *filename) {
    RrnSect *sect = NULL;
    Link *link;

    /* rrn_sect_parse_file() doesn't modify the param. */
    sect = rrn_sect_parse_file((char *)filename);
    if (!sect)
        return;

    link = malloc(sizeof(Link));
    link->reg.sect = sect;
    link->next = NULL;
    link->prev = NULL;

    RRN_HT_APPEND(orphans_head, orphans_tail, link);
}

static void process_file(const char *filename) {
    RrnReg *reg;
    Link *link;

    /* rrn_reg_parse_file() doesn't modify the param. */
    reg = rrn_reg_parse_file((char *)filename);
    if (!reg)
        return;

    if (handle_duplicate(reg)) {
        return;
    }

    link = malloc(sizeof(Link));
    link->reg.reg = reg;
    link->next = NULL;

    RRN_HT_APPEND(head, tail, link);
}

static void insert_orphans() {
    Link *sect = orphans_head;

    while (sect) {
        Link *iter = head;

        while (iter) {
            if (!strncmp(iter->reg.reg->identifier, sect->reg.sect->owner,
                         strlen(iter->reg.reg->identifier))) {
                break;
            }
            iter = iter->next;
        }
        if (iter) {
            sect->reg.sect =
                rrn_reg_add_sections(iter->reg.reg, sect->reg.sect);
            if (sect->reg.sect == NULL) {
                Link *tmp = sect->next;
                RRN_HT_REMOVE(orphans_head, orphans_tail, sect);
                free(sect);
                sect = tmp;
            }
        } else {
            sect->reg.sect->priority++;
            sect = sect->next;
        }
    }
}

static RrnSect *reverse_child(RrnSect *child) {
    RRN_LIST_REVERSE(child);
    for (RrnSect *iter = child; iter; iter = iter->next) {
        iter->children = reverse_child(iter->children);
    }
    return child;
}

static void reverse_children() {
    for (Link *iter = head; iter; iter = iter->next) {
        iter->reg.reg->children = reverse_child(iter->reg.reg->children);
    }
}

RrnReg *rrn_find_from_name(char *name) {
    rrn_init();

    for (Link *iter = head; iter; iter = iter->next) {
        if (iter->reg.reg->name && !strcmp(iter->reg.reg->name, name))
            return iter->reg.reg;
    }

    return NULL;
}

RrnReg *rrn_find_from_ghelp(char *ghelp) {
    Link *iter;

    rrn_init();
    iter = head;

    while (iter) {
        if (iter->reg.reg->ghelp_name &&
            !strcmp(iter->reg.reg->ghelp_name, ghelp))
            return iter->reg.reg;
        iter = iter->next;
    }

    return NULL;
}

void rrn_shutdown() {
    RRN_HT_FREE(head, tail, reg_link_free);
    RRN_HT_FREE(orphans_head, orphans_tail, sect_link_free);
    rrn_language_shutdown();
    return;
}
