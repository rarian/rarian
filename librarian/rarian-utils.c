/*
 * rarian-utils.c
 * This file is part of Rarian
 *
 * Copyright (C) 2006 - Don Scorgie
 *
 * Rarian is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rarian is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#ifdef HAVE_MALLOC_H
#include <malloc.h>
#endif /* HAS_MALLOC_H */
#include "rarian-utils.h"

#define BUF_READ_SIZE 1024

char *rrn_chug(char *string) {
    char *start;

    for (start = string; *start && isspace(*start); start++)
        ;

    memmove(string, start, strlen(start) + 1);

    return string;
}

char *rrn_chomp(char *string) {
    int len;

    len = strlen(string);

    while (len--) {
        if (isspace(string[len]))
            string[len] = '\0';
        else
            break;
    }

    return string;
}

char *rrn_strndup(char *string, int len) {
    char *new_str = NULL;

    if (string) {
        new_str = (char *)calloc(sizeof(char), len + 1);
        strncpy(new_str, string, len);
        new_str[len] = '\0';
    } else
        new_str = NULL;

    return new_str;
}

int rrn_read_line(char **buf, size_t *buf_len, FILE *file) {
    if (!*buf) {
        *buf_len = BUF_READ_SIZE;
        *buf = malloc(*buf_len * sizeof(char));
    }

    int read_idx = 0;
    while (fgets(*buf + read_idx, *buf_len - read_idx, file)) {
        read_idx = strlen(*buf);
        if ((*buf)[read_idx - 1] == '\n') {
            break;
        }
        /* Need more space to read in more data. */
        *buf_len = *buf_len + BUF_READ_SIZE;
        *buf = realloc(*buf, *buf_len * sizeof(char));
        if (!*buf)
            abort();
    }
    return read_idx;
}

char *rrn_strconcat(const char *first, ...) {
    size_t len = strlen(first);
    size_t write_idx = 0;
    const char *next = first;
    char *ret = NULL;
    va_list args;

    va_start(args, first);
    while (next) {
        len += strlen(next);
        next = va_arg(args, const char *);
    }
    va_end(args);

    ret = malloc(len + 1);

    va_start(args, first);
    next = first;
    while (next) {
        strcpy(ret + write_idx, next);
        write_idx += strlen(next);
        next = va_arg(args, const char *);
    }
    va_end(args);

    return ret;
}

size_t rrn_str_count(const char *in, char c) {
    const char *iter = strchr(in, c);
    size_t num = 0;

    while (iter) {
        num++;
        iter = strchr(iter + 1, c);
    }

    return num;
}

char **rrn_str_split(const char *in, char c) {
    size_t nstrs = rrn_str_count(in, c) + 1;
    char **ret = malloc(sizeof(char *) * (nstrs + 1));
    size_t idx = 0;
    /* In isn't modified, but it needs to be compatible with rrn_strndup() */
    char *start = (char *)in;
    char *end = strchr(in, c);
    while (end) {
        ret[idx] = rrn_strndup(start, end - start);
        start = end + 1;
        end = strchr(start, c);
        idx++;
    }

    ret[idx] = strdup(start);
    ret[nstrs] = NULL;
    return ret;
}

void rrn_freev(char **in) {
    if (in) {
        for (int i = 0; in[i]; ++i) {
            free(in[i]);
        }
        free(in);
    }
}
