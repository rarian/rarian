#!/bin/bash

set -xe

dnf install -y 'dnf-command(config-manager)'
dnf config-manager --set-enabled crb

dnf install -y epel-release

# Clear the tsflags config so that docs are installed.
dnf install -y \
    --setopt='tsflags=' \
    autoconf \
    automake \
    bzip2 \
    check-devel \
    diffutils \
    info \
    git \
    gcc \
    gcc-c++ \
    libtool \
    make \
    man-db \
    man-pages \
    tinyxml2-devel

