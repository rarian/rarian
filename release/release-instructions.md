# Release Instructions

- Create the release tarball
    - Make a branch, e.g. `{version}-release`
    - Update Changelog
    - Update NEWS
    - Create a merge request, merge it
    - Create a tag with the version, leave the message blank to make a lightweight tag
- Update the webpage
    - Wait for the tag pipeline to finish
    - Download the release tarball, not the source code archive
    - Use md5sum to get the hash of the tarball
    - Create a branch, e.g. `{version}-web`
    - Update webpage/index.html
    - Create a merge request, merge it