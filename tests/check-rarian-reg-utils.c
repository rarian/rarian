#define I_KNOW_RARIAN_0_8_IS_UNSTABLE
#include "check-rarian.h"
#include "rarian-list-utils.h"
#include "rarian-reg-utils.h"
#include "rarian-utils.h"
#include <check.h>
#include <stdlib.h>

static inline char *get_test_path(const char *filename) {
    char *data_path = getenv("RARIAN_TEST_DATA_PATH");
    ck_assert_ptr_nonnull(data_path);
    return rrn_strconcat(data_path, "/", filename, NULL);
}

START_TEST(test_rrn_reg) {
    char *fname = get_test_path("beanstalk.document");
    RrnReg *reg = rrn_reg_parse_file(fname);
    free(fname);

    ck_assert_ptr_nonnull(reg);
    ck_assert_str_eq("The Beanstalk Manual", reg->name);
    ck_assert_ptr_nonnull(reg->categories);
    ck_assert_str_eq("Beanstalks", reg->categories[0]);
    ck_assert_str_eq("GNOME", reg->categories[1]);
    ck_assert_str_eq("file:///usr/share/gnome/help/beanstalk/C/beanstalk.xml",
                     reg->uri);
    ck_assert_ptr_null(reg->categories[2]);

    /* Check sections. */
    ck_assert_ptr_nonnull(reg->children);
    RrnSect *first = reg->children;
    ck_assert_str_eq("Looking after your beanstalk", first->name);
    ck_assert_str_eq("Care", first->identifier);
    /* Checks the translation from relative to absolute path. */
    ck_assert_str_eq("file:///usr/share/gnome/help/beanstalk/C/care.xml",
                     first->uri);

    ck_assert_ptr_nonnull(first->next);
    RrnSect *second = first->next;
    ck_assert_str_eq("Growing Instructions", second->name);
    ck_assert_str_eq("Growing", second->identifier);
    ck_assert_str_eq("file:///usr/share/gnome/help/beanstalk/C/Growing.xml",
                     second->uri);

    ck_assert_ptr_null(second->next);
    rrn_reg_free(reg);
}
END_TEST

START_TEST(test_rrn_reg_sect_parse) {
    char *fname = get_test_path("beanstalk-climbing.section");
    RrnSect *sect = rrn_sect_parse_file(fname);
    free(fname);

    ck_assert_ptr_nonnull(sect);
    ck_assert_str_eq("Trimming dead leafs", sect->name);
    ck_assert_str_eq("Trimming", sect->identifier);
    ck_assert_str_eq(
        "file:///opt/gnome-cvs/share/gnome/help/C/beanstalk-trimming.xml",
        sect->uri);

    ck_assert_ptr_nonnull(sect->next);
    RrnSect *second = sect->next;
    ck_assert_str_eq("Climbing your beanstalk", second->name);
    ck_assert_str_eq("Climbing", second->identifier);
    ck_assert_str_eq(
        "file:///opt/gnome-cvs/share/gnome/help/C/beanstalk-climbing.xml",
        second->uri);

    ck_assert_ptr_nonnull(second->children);
    RrnSect *child = second->children;
    ck_assert_str_eq("Safety Precautions", child->name);
    ck_assert_str_eq("Safety", child->identifier);
    ck_assert_str_eq(
        "file:///opt/gnome-cvs/share/gnome/help/C/beanstalk-climb-safely.xml",
        child->uri);
    ck_assert_ptr_null(child->next);

    ck_assert_ptr_null(second->next);

    RRN_LIST_FREE(sect, rrn_sect_free);
}
END_TEST

Suite *rarian_reg_utils_suite() {
    Suite *s = suite_create("RarianRegUtils");
    TCase *tc_rrn_reg = tcase_create("RrnReg");

    tcase_add_test(tc_rrn_reg, test_rrn_reg);
    tcase_add_test(tc_rrn_reg, test_rrn_reg_sect_parse);
    suite_add_tcase(s, tc_rrn_reg);

    return s;
}
