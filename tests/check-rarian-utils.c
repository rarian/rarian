
#include "rarian-utils.h"
#include <check.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TEST_BUF_LEN 1024

static void write_file_data(const char *path, const char *content) {
    FILE *fp = fopen(path, "w");
    if (!fp)
        abort();
    fputs(content, fp);
    fclose(fp);
}

static void fill_random_chars(char *buf, size_t len) {
    int range = 'z' - 'a';
    int divisor = (RAND_MAX + 1u) / range;

    for (size_t i = 0; i < len; ++i) {
        buf[i] = 'a' + rand() / divisor;
    }
}

START_TEST(test_rrn_chug) {
    char *tmp = strdup("  Leading string");
    char *res = rrn_chug(tmp);
    ck_assert_str_eq(res, "Leading string");
    free(tmp);
}
END_TEST

START_TEST(test_rrn_chomp) {
    char *tmp = strdup("Trailing string  ");
    char *res = rrn_chomp(tmp);
    ck_assert_str_eq(res, "Trailing string");
    free(tmp);
}
END_TEST

START_TEST(test_rrn_strndup) {
    char *tmp = rrn_strndup("12345", 3);
    ck_assert_str_eq(tmp, "123");
    free(tmp);
}
END_TEST

START_TEST(test_rrn_strconcat) {
    char *res = rrn_strconcat("one", "two", NULL);
    ck_assert_str_eq(res, "onetwo");
    free(res);
}
END_TEST

START_TEST(test_rrn_read_line) {
    const char *test_file = "test_rrn_read_line.txt";
    char *first_line = "first line\n";
    size_t second_len = TEST_BUF_LEN * 3 / 2;
    char *second_line = malloc(second_len);
    char *content = NULL;
    char *buf = NULL;
    size_t buf_len = 0;

    fill_random_chars(second_line, second_len - 2);
    second_line[second_len - 2] = '\n';
    second_line[second_len - 1] = '\0';

    content = rrn_strconcat(first_line, second_line, NULL);
    write_file_data(test_file, content);

    FILE *fp = fopen(test_file, "r");
    ck_assert_ptr_nonnull(fp);
    int first_read = rrn_read_line(&buf, &buf_len, fp);
    ck_assert_int_eq(first_read, strlen(first_line));
    ck_assert_int_eq(TEST_BUF_LEN, buf_len);
    ck_assert_str_eq(first_line, buf);

    int second_read = rrn_read_line(&buf, &buf_len, fp);
    ck_assert_int_eq(second_read, strlen(second_line));
    ck_assert_int_eq(TEST_BUF_LEN * 2, buf_len);
    ck_assert_str_eq(second_line, buf);

    free(second_line);
    free(content);
}
END_TEST

START_TEST(test_rrn_str_count) {
    ck_assert_int_eq(0, rrn_str_count("test", 'c'));
    ck_assert_int_eq(0, rrn_str_count("", 'c'));
    ck_assert_int_eq(2, rrn_str_count("test", 't'));
}
END_TEST

START_TEST(test_rrn_str_split_empty) {
    char **ret = rrn_str_split("", ';');
    ck_assert_ptr_nonnull(ret);
    ck_assert_str_eq(ret[0], "");
    ck_assert_ptr_null(ret[1]);
    rrn_freev(ret);
}
END_TEST

START_TEST(test_rrn_str_split_no_delim) {
    char **ret = rrn_str_split("test", ';');
    ck_assert_ptr_nonnull(ret);
    ck_assert_str_eq(ret[0], "test");
    ck_assert_ptr_null(ret[1]);
    rrn_freev(ret);
}
END_TEST

START_TEST(test_rrn_str_split_one_delim) {
    char **ret = rrn_str_split("test1;test2", ';');
    ck_assert_ptr_nonnull(ret);
    ck_assert_str_eq(ret[0], "test1");
    ck_assert_str_eq(ret[1], "test2");
    ck_assert_ptr_null(ret[2]);
    rrn_freev(ret);
}
END_TEST

START_TEST(test_rrn_str_split_trailing_delim) {
    char **ret = rrn_str_split("test1;", ';');
    ck_assert_ptr_nonnull(ret);
    ck_assert_str_eq(ret[0], "test1");
    ck_assert_str_eq(ret[1], "");
    ck_assert_ptr_null(ret[2]);
    rrn_freev(ret);
}
END_TEST

Suite *rarian_utils_suite() {
    Suite *s = suite_create("RarianUtils");
    TCase *tc_chug;
    TCase *tc_chomp;
    TCase *tc_strdup;
    TCase *tc_strconcat;
    TCase *tc_readline;
    TCase *tc_str_count;
    TCase *tc_str_split;

    tc_chug = tcase_create("Chug");
    tcase_add_test(tc_chug, test_rrn_chug);
    suite_add_tcase(s, tc_chug);

    tc_chomp = tcase_create("Chomp");
    tcase_add_test(tc_chomp, test_rrn_chomp);
    suite_add_tcase(s, tc_chomp);

    tc_strdup = tcase_create("strndup");
    tcase_add_test(tc_strdup, test_rrn_strndup);
    suite_add_tcase(s, tc_strdup);

    tc_strconcat = tcase_create("strconcat");
    tcase_add_test(tc_strconcat, test_rrn_strconcat);
    suite_add_tcase(s, tc_strconcat);

    tc_readline = tcase_create("readline");
    tcase_add_test(tc_readline, test_rrn_read_line);
    suite_add_tcase(s, tc_readline);

    tc_str_count = tcase_create("str_count");
    tcase_add_test(tc_str_count, test_rrn_str_count);
    suite_add_tcase(s, tc_str_count);

    tc_str_split = tcase_create("str_split");
    tcase_add_test(tc_str_split, test_rrn_str_split_empty);
    tcase_add_test(tc_str_split, test_rrn_str_split_no_delim);
    tcase_add_test(tc_str_split, test_rrn_str_split_one_delim);
    tcase_add_test(tc_str_split, test_rrn_str_split_trailing_delim);
    suite_add_tcase(s, tc_str_split);

    return s;
}
