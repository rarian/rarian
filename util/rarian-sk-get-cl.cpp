/*
 * rarian-sk-get-cl.cpp
 * This file is part of Rarian
 *
 * Copyright (C) 2006 - Don scorgie
 *
 * Rarian is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rarian is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rarian; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

#define I_KNOW_RARIAN_0_8_IS_UNSTABLE
#include "rarian-utils.h"
#include "rarian.h"
#include "tinyxml2.h"
#include <sys/stat.h>
#include <sys/types.h>

#define BASE_FORMAT "/tmp/scrollkeeper-%s"
#define FNAME_FORMAT BASE_FORMAT "/contents.%d"

static int id = 0;

void print_usage(char *proc_name) {
    printf("Usage: %s <LOCALE> <CAT TREE NAME>\n", proc_name);
    exit(0);
}

char *get_attribute(tinyxml2::XMLElement *elem) {
    const tinyxml2::XMLAttribute *pAttrib = elem->FirstAttribute();
    char *cat = NULL;

    if (strcmp(pAttrib->Value(), "")) {
        char *new_cat = NULL;
        char *iter = NULL;
        char *st_iter = NULL;
        char *tmp = NULL;

        cat = strdup(pAttrib->Value());
        /* Fix category to mimic scrollkeeper */
        new_cat = (char *)calloc(sizeof(char), strlen(cat));

        st_iter = cat;
        iter = cat;
        while (iter && *iter) {
            while (iter && *iter && *iter != '|') {
                iter++;
            }
            tmp = rrn_strndup(st_iter, (iter - st_iter));
            if (*new_cat) {
                strcat(new_cat, tmp);
            } else {
                strcpy(new_cat, tmp);
            }
            free(tmp);
            if (*iter == '|')
                iter++;
            else
                break;
            st_iter = iter;
        }
        elem->SetAttribute("categorycode", new_cat);
        free(new_cat);
    }
    return cat;
}

int get_docs(RrnReg *reg, tinyxml2::XMLNode *pParent) {
    tinyxml2::XMLDocument *owningDoc = pParent->GetDocument();
    if (reg->omf_location) {
        tinyxml2::XMLElement *doc = owningDoc->NewElement("doc");
        tinyxml2::XMLElement *entry;
        tinyxml2::XMLElement *text;
        char *tmp;

        doc->SetAttribute("id", id);
        id++;

        /* doctitle */
        entry = owningDoc->NewElement("doctitle");
        entry->InsertEndChild(owningDoc->NewText(reg->name));
        doc->InsertEndChild(entry);

        /* docomf */
        entry = owningDoc->NewElement("docomf");
        entry->InsertEndChild(owningDoc->NewText(reg->omf_location));
        doc->InsertEndChild(entry);

        /* docsource */
        tmp = reg->uri + 7; // Remove file://
        entry = owningDoc->NewElement("docsource");
        entry->InsertEndChild(owningDoc->NewText(tmp));
        doc->InsertEndChild(entry);

        /* docformat */
        entry = owningDoc->NewElement("docformat");
        entry->InsertEndChild(owningDoc->NewText(reg->type));
        doc->InsertEndChild(entry);

        /* docseriesid */
        tmp = reg->identifier + 17; // remove org.scrollkeeper.
        entry = owningDoc->NewElement("docseriesid");
        entry->InsertEndChild(owningDoc->NewText(tmp));
        doc->InsertEndChild(entry);
        pParent->InsertEndChild(doc);
    }

    return 0;
}

void process_node(tinyxml2::XMLNode *pParent) {
    tinyxml2::XMLNode *pChild;
    tinyxml2::XMLText *pText;
    int num;

    if ((pParent->ToDocument() == nullptr) &&
        (pParent->ToElement() != nullptr)) {
        if (!strcmp(pParent->Value(), "sect")) {
            char *cat = get_attribute(pParent->ToElement());
            rrn_for_each_in_category((RrnForeachFunc)get_docs, cat, pParent);
            free(cat);
        }
    }

    for (pChild = pParent->FirstChild(); pChild != 0;
         pChild = pChild->NextSibling()) {
        process_node(pChild);
    }
}

/* Find a suitable file to dump out output to.
 * for now, we're always going to the same file
 */
char *find_dump_name() {
    char *filename = NULL;
    const char *user = getenv("USERNAME");
    int last = 0;
    unsigned int lasttime = 0;
    int init = 1;

    if (!user) {
        /* if USERNAME isn't set, we use the "default" username: UNKNOWN */
        user = "UNKNOWN";
    }

    filename =
        (char *)malloc(sizeof(char) * (sizeof(FNAME_FORMAT) + strlen(user)));
    sprintf(filename, BASE_FORMAT, user);
    mkdir(filename, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    for (int i = 0; i < 5; i++) {
        struct stat sbuf;
        sprintf(filename, FNAME_FORMAT, user, i);
        if (stat(filename, &sbuf) == -1) {
            last = i;
            break;
        }
        if (sbuf.st_mtime < lasttime || init) {
            init = 0;
            last = i;
            lasttime = sbuf.st_mtime;
        }
    }
    sprintf(filename, FNAME_FORMAT, user, last);

    return filename;
}

int main(int argc, char *argv[]) {
    int skip = 0;

    if (argc < 3 || argc > 4) {
        print_usage(argv[0]);
    }
    if (!strcmp(argv[1], "-v")) {
        skip = 1;
    }

    if (strcmp(argv[2 + skip], "scrollkeeper_extended_cl.xml") &&
        strcmp(argv[2 + skip], "scrollkeeper_cl.xml")) {
        print_usage(argv[0]);
    }
    tinyxml2::XMLError loadok;

    rrn_set_language(argv[1 + skip]);

    tinyxml2::XMLDocument doc;
    loadok = doc.LoadFile(DATADIR "/librarian/rarian-sk-cl.xml");

    if (loadok != tinyxml2::XML_SUCCESS) {
        fprintf(stderr, "ERROR: Cannot parse template file.  Aborting.\n");
        exit(2);
    }

    tinyxml2::XMLNode *pParent = doc.FirstChild();

    process_node(pParent);

    char *filename = find_dump_name();
    doc.SaveFile(filename);
    printf("%s\n", filename);

    exit(0);
}
